<?php declare(strict_types=1);

use Drupal\views_s3_export\Exception\LoadingExportServiceByS3KeyException;
use Drupal\views_s3_export\ViewExportServicePartial;

/**
 * Implements hook_s3fs_upload_params_alter().
 */
function views_s3_export_s3fs_upload_params_alter(array &$uploadParams) {
  /** @var \Drupal\views_s3_export\ViewExportServicePartial $feedExportPartial */
  $feedExportPartial = \Drupal::service('views_s3_export.partial');

  try {
    // Get a complete feed export service using the upload's S3 Key.
    $feedExport = $feedExportPartial->apply_(s3Key: $uploadParams['Key']);

    $uploadParams['CacheControl'] = $feedExport->getCacheControl();
    $uploadParams['ContentType'] =
      $feedExport->getContentType() ?? $uploadParams['ContentType'];
  } catch (LoadingExportServiceByS3KeyException $e) {
    // No action necessary, this file upload is not a View export.
  }
}

/**
 * Implements hook_rebuild().
 */
function views_s3_export_rebuild() {
  ViewExportServicePartial::cacheRebuild();
}
