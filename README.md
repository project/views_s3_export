# Views S3 export

- Do you have an [RSS Feed](https://www.drupal.org/project/views_rss) in a View, and need to export it to Amazon S3?
- Do you have a View of some other data type, like CSV or XML, that you need to export to Amazon S3?
- Are your site's feeds being slammed, or is access to them being throttled by your host?
- Are your MailChimp newsletters going out saying _NO RSS ITEMS FOUND_?
- Do you need to do any of the above, but with an S3 compatible service?

As long as you have access to [Drush](https://www.drush.org) and an AWS account, this module might help!

_Views S3 export_ provides a Drush command for exporting a [View](https://www.drupal.org/docs/8/core/modules/views) to a file on [Amazon S3](https://aws.amazon.com/s3/) (or a compatible service). From there, the file can be served from [CDN](https://aws.amazon.com/cloudfront), _fast_.

## Features

- Doesn't require any alterations to your Views; unlike [Views data export](https://www.drupal.org/project/views_data_export) it doesn't require a _View display_ to itself.
- Change the [Cache-Control HTTP header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control) S3/CloudFront will use. For example, `max-age=300, public`.
- Change the [Content-Type HTTP header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type) S3/CloudFront will use. For example, `application/rss+xml`.
- Choose any _View_ and _View display_ to export.

## Post-Installation

The Drush command may be run like this: `drush views:s3-export view_id display_id` while running: `drush --help views:s3-export` shows the available options.

Note: the command always uses the _Anonymous_ user when executing the View (please create a feature request if you have a different use-case).

### Platform.sh example

As an example of how to use it, Woolwich Web Works is using Views S3 export on a production site (hosted on Platform.sh) in the following way.

_.platform.app.yml_:

```yaml
crons:
  newsletter:
    spec: '*/5 * * * *'
    commands:
      start: "cd web ; drush --uri=https://example.com --content-type='application/rss+xml' views:s3-export newsletter_feed rss_feed"
```

## Supporting this Module

The developers of this module are available for hire at [Woolwich Web Works](https://www.woolwichweb.works). If you have budget for developing a feature you need, if you're confused by how S3 and CloudFront and Drupal fit together, or need to delegate the work of setting it up, get in touch!

