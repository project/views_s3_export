<?php declare(strict_types=1);

/**
 * @file
 * Classes and supporting types for exporting a View display.
 */

namespace Drupal\views_s3_export;

use Drupal\Core\File\Exception\FileException;
use Drupal\Core\State\State;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\views_s3_export\Exception\LoadingExportServiceByS3KeyException;
use Drupal\views_s3_export\Exception\LockAcquiringException;
use Drupal\s3fs\StreamWrapper\S3fsStream;
use Drupal\s3fs\Traits\S3fsPathsTrait;
use Drupal\views\ViewEntityInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\ViewExecutableFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Id of Url and S3 details stored in Drupal's cache.
 */
define('URL_CACHE_ID', 'views_s3_export:urls');

/**
 * Supported export schemes, e.g. 's3://', 'public://'.
 */
enum ExportScheme: string {
  case S3 = 's3://';
  case Public = 'public://';
  case Private = 'private://';
}

/**
 * Url and S3 Key details for storage in Drupal's cache.
 */
final class CachedUrl {
  public function __construct(
    public readonly string $s3Key,
    public readonly Url $url,
    public readonly string $viewId,
    public readonly string $viewDisplayId,
    public readonly ExportScheme $exportScheme,
  ) {
  }
}

/**
 * Partially applied export service.
 *
 * Call `apply()` with View and display ids to get the full export service.
 */
class ViewExportServicePartial {
  public function __construct(
    protected readonly ViewExecutableFactory $viewsExecutable,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly RendererInterface $renderer,
    protected readonly FileSystemInterface $fileSystem,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly RequestStack $requestStack,
    protected readonly State $state,
    protected readonly StreamWrapperManagerInterface $streamWrapper,
  ) {
  }

  /**
   * Apply a View id, and display id to get a full export service.
   *
   * @param string $viewId
   * Id of the View to export.
   * @param string $viewDisplayId
   * Id of the View's display to export.
   *
   * @return ViewExportService
   * Full export service, with `export()` function for exporting the View.
   */
  public function apply(
    string $viewId,
    string $viewDisplayId,
    ExportScheme $exportScheme = ExportScheme::S3,
  ): ViewExportService {
    return new ViewExportService(
      $this->viewsExecutable,
      $this->entityTypeManager,
      $this->renderer,
      $this->fileSystem,
      $this->configFactory,
      $this->requestStack,
      $this->state,
      $this->streamWrapper,
      $this,
      $viewId,
      $viewDisplayId,
      $exportScheme,
    );
  }

  /**
   * Apply an S3 key to this partial get a full export service.
   *
   * Requires there to already be a `CachedUrl` for the corresponding export
   * service in Drupal's cache. If this throws a
   * `LoadingExportServiceByS3KeyException`, the cache did not contain an entry
   * for the provided S3 key, and `apply()` should be used instead.
   *
   * @param string $s3Key
   * Return the full export service for this S3 Key.
   *
   * @throws \Drupal\views_s3_export\Exception\LoadingExportServiceByS3KeyException
   */
  public function apply_(string $s3Key): ViewExportService {
    $cache = \Drupal::cache();

    /** @var CachedUrl[] */
    $item = $cache->get(URL_CACHE_ID)->data ?: [];

    $cachedUrl = match (true) {
      isset($item[$s3Key]) && $item[$s3Key] instanceof CachedUrl => $item[
        $s3Key
      ],
      default => throw new LoadingExportServiceByS3KeyException(
        'Discovering View by S3 Key failed, the lookup cache did not contain a mapping for the provided Key.',
      ),
    };

    return $this->apply(
      viewId: $cachedUrl->viewId,
      viewDisplayId: $cachedUrl->viewDisplayId,
      exportScheme: $cachedUrl->exportScheme,
    );
  }

  /**
   * Respond to a system cache rebuild.
   *
   * Ensures there is an (empty) cache entry for Views exports.
   *
   * @see views_s3_export_rebuild()
   */
  public static function cacheRebuild(): void {
    $cache = \Drupal::cache();
    $cache->set(
      cid: URL_CACHE_ID,
      data: [],
      expire: \Drupal\Core\Cache\Cache::PERMANENT,
      tags: ['views_data'],
    );
  }
}

/**
 * Full view export service.
 *
 * Note on the relationship between this and `ViewExportServicePartial`:
 * For convenience, it's easiest to use dependency injection to load a
 * `ViewExportServicePartial` and use `apply()`, with the View and display
 * name (and any other dependencies) to get this full export service.
 *
 * The classes are separate for type checking and Intellisense purposes.
 */
class ViewExportService {
  use S3fsPathsTrait;

  /**
   * The View used to generate an export.
   */
  protected ViewEntityInterface $viewEntity;

  /**
   * This class alters the Symfony Request stack, this stores the original.
   */
  private readonly Request $originalRequest;

  /**
   * Key/machine name used internally for State and Lock API keys.
   */
  protected readonly string $exportName;

  /**
   * The length of time to lock an View display for export.
   */
  private const lockTimeout = 60.0;

  public function __construct(
    protected readonly ViewExecutableFactory $viewsExecutable,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly RendererInterface $renderer,
    protected readonly FileSystemInterface $fileSystem,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly RequestStack $requestStack,
    protected readonly State $state,
    protected readonly StreamWrapperManagerInterface $streamWrapper,
    protected readonly ViewExportServicePartial $partial,
    protected readonly string $viewId,
    protected readonly string $viewDisplayId,
    public readonly ExportScheme $exportScheme = ExportScheme::S3,
  ) {
    $this->originalRequest = $requestStack->getCurrentRequest();
    $viewStore = $entityTypeManager->getStorage('view');

    $viewEntity = $viewStore->load($viewId);

    if ($viewEntity instanceof ViewEntityInterface) {
      $this->viewEntity = $viewEntity;
    } else {
      throw new \Exception('View ("' . $viewId . '") not found.');
    }

    $this->exportName = 'views_s3_export_' . $viewId . '_' . $viewDisplayId;
  }

  /**
   * Get the View related to this export.
   */
  protected function getExportView(): ViewExecutable {
    $view = $this->viewsExecutable->get($this->viewEntity);
    $view->setDisplay($this->viewDisplayId);

    return $view;
  }

  /**
   * Get View display export path.
   *
   * @throws \Exception()
   */
  protected function getExportViewPath(): string {
    $path = $this->getExportView()->getPath();

    if (isset($path)) {
      return $path;
    } else {
      throw new \Exception(
        'Could not get path from View: `' .
          $this->viewId .
          '` and display: `' .
          $this->viewDisplayId .
          '`.',
      );
    }
  }

  /**
   * The URI S3 should write to, for use with file system functions.
   *
   * Note: S3fs may change this internally, adding 's3fs-public', 's3', etc.
   * to the path before the file is actually saved.
   */
  public function getExportUri(): string {
    $uri = $this->resolvePath(
      $this->exportScheme->value . $this->getExportViewPath(),
    );

    return $uri;
  }

  /**
   * Creates a new request with the View's path and makes it current.
   *
   * This ensures the feed's self-references have the correct path.
   *
   * @see endFeedRequest()
   *  Used in conjunction with this to setup/tead-down a feed request.
   */
  private function startExportRequest(): static {
    $path = $this->getExportViewPath();
    $newRequest = Request::create($path);

    // Drupal core Views assumes a session.
    // @see Drupal\views\ViewExecutable::getExposedInput() for the
    // offending code.
    $newRequest->setSession(
      new \Symfony\Component\HttpFoundation\Session\Session()
    );

    $this->requestStack->pop();
    $this->requestStack->push($newRequest);

    return $this;
  }

  /**
   * Revert Symfony to the original Request.
   *
   * @see startFeedRequest()
   *   Used in conjuntion with this to setup/tear-down a feed request.
   */
  private function endExportRequest(): static {
    $this->requestStack->pop();
    $this->requestStack->push($this->originalRequest);

    return $this;
  }

  /**
   * Get the URL of the View display, from S3's (not the site's) perspective.
   *
   * @see \Drupal\views_s3_export\ViewExportService::getExportSiteUrl()
   */
  public function getExportCdnUrl(): Url {
    $uri = $this->getExportUri();
    $streamWrapper = $this->streamWrapper->getViaUri($uri);

    if ($streamWrapper instanceof S3fsStream) {
      return Url::fromUri($streamWrapper->getExternalUrl())->setAbsolute();
    } else {
      throw new \UnexpectedValueException(
        "The stream wrapper for '$uri' is not an S3fsStream",
      );
    }
  }

  /**
   * Get the URL of the View display, from the site's (not S3's) perspective.
   *
   * @see \Drupal\views_s3_export\ViewExportService::getExportCdnUrl()
   */
  public function getExportSiteUrl(): Url {
    $viewPath = $this->getExportViewPath();
    $scheme = $this->originalRequest->getScheme();
    $domain = $this->originalRequest->getHost();
    $port = $this->originalRequest->getPort();

    // Construct the base URL of the site.
    $siteBaseUrl =
      $scheme . '://' . $domain . (in_array($port, [80, 443]) ? '' : $port);

    return Url::fromUri("{$siteBaseUrl}/{$viewPath}")->setAbsolute();
  }

  /**
   * Chainable function for acquiring the lock, prevents parallel generation.
   *
   * Note: this only locks exporting for one View and display, so multiple
   * exports may be performed in parallel, as long as they're not for the same
   * View and display.
   *
   * @throws \Drupal\views_s3_export\Exception\LockAcquiringException
   */
  protected function acquireLock(): static {
    $lock = \Drupal::lock();

    if ($lock->acquire(name: $this->exportName, timeout: static::lockTimeout)) {
      return $this;
    } else {
      throw new LockAcquiringException();
    }
  }

  /**
   * Chainable function for releasing the export lock.
   */
  protected function releaseLock(): static {
    $lock = \Drupal::lock();
    $lock->release(name: $this->exportName);

    return $this;
  }

  /**
   * Set the Cache-Control header for the file in S3/Cloudfront.
   *
   * @param string $cacheControl
   * Cache-Control header value, e.g. `public, max-age=300`.
   *
   * @return static
   */
  protected function setCacheControl(string $cacheControl): static {
    $state = \Drupal::state();
    $state->set(
      key: $this->exportName . '.cache_control',
      value: $cacheControl,
    );

    return $this;
  }

  /**
   * Get the Cache-Control header for the file in S3/Cloudfront.
   *
   * @return string
   */
  public function getCacheControl(): string {
    $state = \Drupal::state();
    $cacheControl = $state->get(key: $this->exportName . '.cache_control');

    if (is_null($cacheControl)) {
      throw new \UnexpectedValueException(
        'Key `' .
          $this->exportName .
          '.cache_control` is missing from state storage',
      );
    } else {
      return $cacheControl;
    }
  }

  /**
   * Set (or unset) the Content-Type header for the file in S3/Cloudfront.
   *
   * @param string|null $contentType
   * The Content-Type or null to unset the file's Content-Type.
   */
  protected function setContentType(string|null $contentType): static {
    $state = \Drupal::state();
    $state->set(key: $this->exportName . '.content_type', value: $contentType);

    return $this;
  }

  /**
   * Get the Content-Type header for the file in S3/Cloudfront (if any).
   *
   * @return string|null The Content-Type as a string, or null.
   */
  public function getContentType(): string|null {
    $state = \Drupal::state();
    return $state->get(key: $this->exportName . '.content_type');
  }

  /**
   * Get all cached exports.
   *
   * Cache data is stored in one array, to avoid cache misses every time
   * a file is uploaded to S3. The array is keyed by S3 file key.
   *
   * @return CachedUrl[]
   */
  private function getCache(): array {
    $cache = \Drupal::cache();
    $getCache = ($cache->get(URL_CACHE_ID) ?: (object) ['data' => []])->data;

    return $getCache;
  }

  /**
   * Add export to the cache.
   *
   * This class has enough internal state to provide every dependency this
   * function has, so it does not require any parameters.
   */
  private function addToCache(): static {
    $cache = \Drupal::cache();
    $fileKey = $this->getS3fsFileKey();

    $cache->set(
      cid: URL_CACHE_ID,
      data: [
        ...$this->getCache(),
        $fileKey => new CachedUrl(
          s3Key: $fileKey,
          url: $this->getExportCdnUrl(),
          exportScheme: $this->exportScheme,
          viewId: $this->viewId,
          viewDisplayId: $this->viewDisplayId,
        ),
      ],
      expire: \Drupal\Core\Cache\Cache::PERMANENT,
      tags: ['views_data'],
    );

    return $this;
  }

  /**
   * Remove export from cache.
   *
   * Export cache data is usually only kept while the export is in progress.
   */
  private function removeFromCache(): static {
    $cache = \Drupal::cache();
    $fileKey = $this->getS3fsFileKey();

    $cache->set(
      cid: URL_CACHE_ID,
      data: array_filter(
        $this->getCache(),
        fn(CachedUrl $cachedUrl) => $cachedUrl->s3Key !== $fileKey,
      ),
      expire: \Drupal\Core\Cache\Cache::PERMANENT,
      tags: ['views_data'],
    );

    return $this;
  }

  /**
   * Export the View display to a file.
   *
   * @return string A string with the path of the resulting file.
   * @throws \Drupal\views_s3_export\Exception\LockAcquiringException
   * @throws \Drupal\Core\File\Exception\FileException
   */
  public function export(
    string $cacheControl = 'public, max-age=300',
    string|null $contentType = null,
  ): string {
    // Perform necessary house-keeping to start the export.
    $this->acquireLock()
      ->setCacheControl($cacheControl)
      ->setContentType($contentType)
      ->startExportRequest()
      ->addToCache();

    $view = $this->getExportView();
    $view->execute();
    $renderArray = $view->render();
    $output = (string) $this->renderer->renderRoot($renderArray);
    $this->endExportRequest();

    $siteFeedUrl = $this->getExportSiteUrl();
    $cdnFeedUrl = $this->getExportCdnUrl();

    $output = str_replace(
      search: $siteFeedUrl->toString(),
      replace: $cdnFeedUrl->toString(),
      subject: $output,
    );

    try {
      $exportResult = $this->fileSystem->saveData(
        $output,
        $this->getExportUri(),
        FileSystemInterface::EXISTS_REPLACE,
      );
    } catch (FileException $e) {
      $this->removeFromCache()->releaseLock();
      throw $e;
    }
    $this->removeFromCache()->releaseLock();

    if (!empty($exportResult)) {
      return $exportResult;
    } else {
      // Sometimes saveData() may return `false` on failure, making its
      // type a lie, guarantee a string return value by throwing when it
      // returns anything else.
      throw new FileException(
        'Saving file to S3 failed, a reason was not given.',
      );
    }
  }

  /**
   * Get the `Key` S3FS module will use in `$uploadParams`.
   *
   * Essential for using S3fs's file upload hooks.
   *
   * This was cribbed (and rewritten) from functions in `S3fsFileService`, that
   * was necessary because `S3fsFileService` does not abstract out its code for
   * obtaining the S3 Key for a file from its file operation functions.
   *
   * @see hook_s3fs_upload_params_alter()
   * @see S3fsFileService::putObject()
   */
  protected function getS3fsFileKey() {
    $destination = $this->resolvePath(
      $this->exportScheme->value . $this->getExportViewPath(),
    );

    $s3Config = $this->configFactory->get('s3fs.settings')->get();

    $scheme = $this->streamWrapper->getScheme($destination);
    $keyPathSuffix = $this->streamWrapper->getTarget($destination);
    $keyPathPrefix = isset($s3Config['root_folder'])
      ? $s3Config['root_folder'] . '/'
      : '';

    $keyPath =
      $keyPathPrefix .
      match ($scheme) {
        'public' => ($s3Config['public_folder'] ?? 's3fs-public') . '/',
        'private' => ($s3Config['private_folder'] ?? 's3fs-private') . '/',
        default => '',
      } .
      $keyPathSuffix;

    return $keyPath;
  }
}
