<?php declare(strict_types=1);

namespace Drupal\views_s3_export\Exception;

class LoadingExportServiceByS3KeyException extends \Exception {
}
