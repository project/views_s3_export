<?php declare(strict_types=1);

namespace Drupal\views_s3_export\Drush\Commands;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Drupal\views_s3_export\ViewExportServicePartial;

/**
 * Drush commandfile for Views S3 Export.
 */
final class ViewsS3ExportCommands extends DrushCommands {
  public function __construct(
    private readonly ViewExportServicePartial $feedExportService,
  ) {
    parent::__construct();
  }

  public static function create(ContainerInterface $container): self {
    return new static($container->get('views_s3_export.partial'));
  }

  /**
   * Export a View display to Amazon S3.
   *
   * @param array{cache-control: string|null, content-type: string|null} $options
   */
  #[CLI\Command(name: 'views:s3-export', aliases: ['vs3'])]
  #[CLI\Argument(name: 'view', description: 'Id of the View to export')]
  #[
    CLI\Argument(
      name: 'display',
      description: 'Display id (from the View) to export',
    ),
  ]
  #[
    CLI\Option(
      name: 'cache-control',
      description: 'Cache-Control header S3/CloudFront should use when serving this View.',
    ),
  ]
  #[
    CLI\Option(
      name: 'content-type',
      description: 'Content-Type header S3/CloudFront should use when serving this View.',
    ),
  ]
  #[
    CLI\Usage(
      name: 'drush views:s3-export newsletter_feed rss_feed',
      description: 'Export a View with id `newsletter_feed` and display id `rss_feed` to Amazon S3.' .
        PHP_EOL .
        'Hint: ids can be found in the URL when editing a View. The URL for this example is' .
        PHP_EOL .
        '`/admin/structure/views/view/newsletter_feed/edit/rss_feed`.',
    ),
  ]
  public function export(
    string $view,
    string $display,
    array $options = ['cache-control' => null, 'content-type' => null],
  ): void {
    try {
      $fullService = $this->feedExportService->apply($view, $display);

      // Run the export and get its path, e.g. `s3://path/to/feed.rss`.
      $path = match (true) {
        isset($options['cache-control']) && isset($options['content-type'])
          => $fullService->export(
          cacheControl: $options['cache-control'],
          contentType: $options['content-type'],
        ),
        isset($options['cache-control']) => $fullService->export(
          cacheControl: $options['cache-control'],
        ),
        default => $fullService->export(contentType: $options['content-type']),
      };

      // Get the URL of the export, so we can display a nice message to
      // the user.
      $url = $fullService->getExportCdnUrl()->toString();

      $this->logger()->success(
        dt('Feed was written to %path and should be available at %url.', [
          '%path' => $path,
          '%url' => $url,
        ]),
      );
    } catch (\Drupal\views_s3_export\Exception\LockAcquiringException $e) {
      $this->logger()->notice(dt('Feed export is already running.'));
    }
  }
}
